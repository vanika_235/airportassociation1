/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline1;

import java.util.Date;

/**
 *
 * @author HP
 */
public class Flight1 {
    private int number;
    private Date date;
    
    public Flight1 (int number, Date date){
        this.number=number;
        this.date=date;
    }
    
    public int getNumber(){
        return this.number;
    }
    
    public Date getDate(){
        return this.date;
    }
            
}
