/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package airline1;

/**
 *
 * @author HP
 */
public class Airplane1 {
     private int id;
    private int numberOfSeats;
    private String make;
    private String model;
    
    public Airplane1 (int id, int numberOfSeats, String make, String model ){
        this.id=id;
        this.numberOfSeats=numberOfSeats;
        this.make=make;
        this.model=model;
    }
   public int getId(){
     return this.id;
    }
    
    public int getNumberOfSeats(){
        return this.numberOfSeats;
        
    }
    
    public String getMake(){
        return this.make;
    }
             
    public String getModel(){
        return this.model;
    }
}


